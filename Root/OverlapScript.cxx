#include "HiggsOverlapStudies/OverlapScript.h"
#include "HGamAnalysisFramework/HgammaIncludes.h"
#include "HGamAnalysisFramework/HGamVariables.h"

// this is needed to distribute the algorithm to the workers
ClassImp(OverlapScript)



OverlapScript::OverlapScript(const char *name)
: HgammaAnalysis(name)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}



OverlapScript::~OverlapScript()
{
  // Here you delete any memory you allocated during your analysis.
}



EL::StatusCode OverlapScript::createOutput()
{
  // Here you setup the histograms needed for you analysis. This method
  // gets called after the Handlers are initialized, so that the systematic
  // registry is already filled.

  TFile *fout = wk()->getOutputFile("MxAOD");
  m_tree = new TTree("physics","physics");
  m_tree->SetDirectory(fout);
  m_tree->Branch("event", &m_event);
  m_tree->Branch("category", &m_category);

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode OverlapScript::execute()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  // Important to keep this, so that internal tools / event variables
  // are filled properly.
  HgammaAnalysis::execute();

  // Clear Variables
  m_event = -999999;
  m_category.clear();

  // Check if diphoton selection is passed 
  if (not var::isPassed()) return EL::StatusCode::SUCCESS;
  m_category.push_back( 9999 + var::catCoup_dev() );

  // HTop Overlap (change in labels and ignore other categories )
  //if (var::catCoup_dev() == 9) m_category.push_back( 82000 );
  //else if (var::catCoup_dev() == 10) m_category.push_back( 82100 );
  //else return EL::StatusCode::SUCCESS;

  m_event = eventInfo()->eventNumber();
  m_tree->Fill();

  //std::cout << "event = " << m_event << "   category = " << m_category[0] << std::endl;

  return EL::StatusCode::SUCCESS;
}
