#directory='root://eosatlas//eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h012pre/overlap'
directory="overlap"
suffix='Overlap.MxAOD.p2501.h012pre.root'
samples="\
PowhegPythia8_ggH125 \
PowhegPythia8_VBF125 \
Pythia8_WH125 \
Pythia8_ZH125 \
aMcAtNloHerwigpp_ttH125"

#samples="PowhegPythia_ttbar"
#suffix='Overlap.MxAOD.p2419.h012pre.root'

for sample in $samples; do
  outdir="output/overlap_${sample}"
  rm -rf ${outdir}

  cmd="runOverlapScript HiggsOverlapStudies/OverlapScript.cfg"
  cmd+=" SampleName: ${sample} InputFile: ${directory}/${sample}.${suffix}"
  cmd+=" OutputDir: ${outdir}"

  #echo $cmd
  $cmd
done

